package com.sprint.forex.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import com.sprint.forex.dto.UserBankDetailsDto;
import com.sprint.forex.entity.UserBankDetails;
import com.sprint.forex.entity.Users;
import com.sprint.forex.exception.UserBankDetailsNotFoundException;
import com.sprint.forex.exception.UsersNotFoundException;
import com.sprint.forex.repository.IUserBankDetailsRepository;
import com.sprint.forex.repository.UsersRepository;

@SpringBootTest
public class UserBankDetailsTest
{
 
	@InjectMocks
	public IUserBankDetailsServiceImpl userBankDetails;
	
	@Mock
	private UsersRepository usersRepository;
	@Mock
	private IUserBankDetailsRepository iUserBankDetailsRepository;
	
	
	@Test
	public  void TestGetSaveuserBankDetailsException()
	{
		
		
		when(iUserBankDetailsRepository.findAll()).thenThrow(UserBankDetailsNotFoundException.class);
		assertThrows(UserBankDetailsNotFoundException.class,()->userBankDetails.getAllBankDetails());
		
	}
	
	
	    @Test
	    public void testSaveBankDetails() {
	        // add user details
	        int userId = 1;
	        Users user = new Users();
	        user.setUsersId(userId);
	        UserBankDetailsDto bankDto = new UserBankDetailsDto();
	        bankDto.setUserId(userId);
	        bankDto.setAccountHolderName("Anuhya Muppavarapu");
	        bankDto.setBankName("HDFC");
	        bankDto.setAccountNumber((long) 1234567890);
	        bankDto.setIfscCode("HDFC1234");
	        bankDto.setContactNumber("1234567890");
	        Mockito.when(usersRepository.findById(userId)).thenReturn(Optional.of(user));
	        UserBankDetails savedBankDetails = new UserBankDetails();
	        savedBankDetails.setId(1);
	        savedBankDetails.setAccountHolderName("Anuhya Muppavarapu");
	        savedBankDetails.setBankName("HDFC");
	        savedBankDetails.setAccountNumber(1234567890);
	        savedBankDetails.setIfscCode("HDFC1234");
	        savedBankDetails.setContactNumber("1234567890");
	        savedBankDetails.setUser(user);
	        Mockito.when(iUserBankDetailsRepository.save(Mockito.any(UserBankDetails.class))).thenReturn(savedBankDetails);

	        // action
	        UserBankDetailsDto savedBankDto =userBankDetails.saveBankDetails(bankDto);

	        // assert Methods
	        assertNotNull(savedBankDto);
	        assertNotNull(savedBankDto.getUserBankId());
	        assertNotNull(savedBankDto.getAccountHolderName());
	        assertNotNull(savedBankDto.getBankName());
	        assertNotNull(savedBankDto.getAccountNumber());
	        assertNotNull(savedBankDto.getIfscCode());
	        assertNotNull(savedBankDto.getContactNumber());
	        assertTrue(savedBankDto.getUserBankId() > 0);
	        assertEquals(1, savedBankDto.getUserBankId());
	        assertEquals("Anuhya Muppavarapu", savedBankDto.getAccountHolderName());
	        assertEquals("HDFC", savedBankDto.getBankName());
	        assertEquals(1234567890, savedBankDto.getAccountNumber());
	        assertEquals("HDFC1234", savedBankDto.getIfscCode());
	        assertEquals("1234567890", savedBankDto.getContactNumber());
	        assertEquals(1, savedBankDto.getUserId()  );
	        
	        
	        
	    }
	    
	    
	    
}
